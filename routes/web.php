<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Auth::routes();

Route::group(['middleware' => ['auth','admin']], function () {//Rutas de Admin

    //Rutas Admin
    Route::get('/dashboard', 'AdminController@Inicio')->name('dashboard');
    
    //CRUD Usuarios
    Route::get('/usuarios', 'AdminController@MirarUsuarios')->name('usuarios.mirar');

    Route::get('/usuarios/editar/{id}', 'AdminController@EditarUsuario')->name('usuarios.editar');
   
    Route::put('/usuarios/editar/{id}', 'AdminController@ActualizarUsuario')->name('usuarios.actualizar');

    Route::delete('/usuarios/{id}', 'AdminController@EliminarUsuario')->name('usuarios.eliminar');

    //CRUD Categorías
    Route::get('/categorias', 'AdminController@MirarCategorias')->name('categorias.mirar');

    Route::get('/categorias/crear','AdminController@VistaCategoria')->name('categorias.crear.vista');

    Route::post('/categorias/crear','AdminController@CrearCategoria')->name('categorias.crear');
    
    Route::get('/categorias/editar/{id}', 'AdminController@EditarCategoria')->name('categorias.editar');

    Route::put('/usuarios/editar/{id}', 'AdminController@ActualizarCategoria')->name('categorias.actualizar');
    
    Route::delete('/categorias/{id}', 'AdminController@EliminarCategoria')->name('categorias.eliminar');

    //CRUD Producto

    Route::get('/productos', 'AdminController@MirarProductos')->name('productos.mirar');

    Route::get('/productos/crear','AdminController@VistaProducto')->name('productos.crear.vista');

    Route::post('/productos/crear','AdminController@CrearProducto')->name('productos.crear');

    Route::get('/productos/editar/{id}', 'AdminController@EditarProducto')->name('productos.editar');

    Route::put('/productos/editar/{id}', 'AdminController@ActualizarProducto')->name('productos.actualizar');

    Route::delete('/productos/{id}', 'AdminController@EliminarProducto')->name('productos.eliminar');
});


Route::group(['middleware' => ['auth','client']], function () {//Rutas de cliente
    //Rutas Clientes
    Route::get('/shop', 'ClientController@Inicio')->name('shop');

    //Rutas de vistas de ropa Hombre
    Route::get('/camisas/hombre/{id}','ClientController@MirarProductosHombre')->name('mirar.camisas.hombre');
    Route::get('/pantalones/hombre/{id}','ClientController@MirarProductosHombre')->name('mirar.pantalones.hombre');
    
    //Rutas de vistas de ropa Mujer
    Route::get('/camisas/mujer/{id}','ClientController@MirarProductosMujer')->name('mirar.camisas.mujer');
    Route::get('/pantalones/mujer/{id}','ClientController@MirarProductosMujer')->name('mirar.pantalones.mujer');

    //Rutas de vistas de carrito
    Route::get('/carrito','CarritoController@Inicio')->name('mirar.carrito');
    
    Route::get('/carrito/agregar-producto/{id}','CarritoController@AgregarProducto')->name('carrito.agregar');

    Route::get('/carrito/eliminar-unidad/{id}','CarritoController@EliminarProducto')->name('carrito.eliminar');

    Route::get('/carrito/pagar/{total}','CarritoController@CrearOrden')->name('carrito.crear.orden');

    Route::get('/carrito/ordenes','CarritoController@MirarOrdenesRealizadas')->name('carrito.ordenes');

    Route::get('/carrito/ordenes','CarritoController@MirarOrdenesRealizadas')->name('carrito.ordenes');

    Route::get('/carrito/ordenes/{id}','CarritoController@MirarDetallesOrden')->name('carrito.ordenes.detalles');

    Route::get('/producto/detalle/{id}','CarritoController@MirarDetalleProducto')->name('producto.detalles');
    
});