<?php

namespace App\Http\Controllers;

use App;
use DB;
use Illuminate\Support\Facades\Auth;

class CarritoController extends Controller
{
    public function Inicio()
    {
        return view('client.carrito');
    }

    public function AgregarProducto($id)
    {
        $producto = App\Producto::findOrfail($id);

        $carrito = session()->get('carrito');

        if (!$carrito) {
            $carrito = [
                $id => [

                    "nombre" => $producto->nombre,
                    "descripcion" => $producto->descripcion,
                    "imagen" => $producto->imagen,
                    "cantidad" => 1,
                    "precio" => $producto->precio,

                ],
            ];

            session()->put('carrito', $carrito);
            return redirect()->back()->with('mensaje', 'Producto agregado a carrito');
        }

        if (isset($carrito[$id])) {
            $carrito[$id]["cantidad"]++;
            session()->put('carrito', $carrito);
            return redirect()->back()->with('mensaje', 'Producto agregado a carrito');
        }

        $carrito[$id] = [
            "nombre" => $producto->nombre,
            "descripcion" => $producto->descripcion,
            "imagen" => $producto->imagen,
            "cantidad" => 1,
            "precio" => $producto->precio,
        ];
        session()->put('carrito', $carrito);
        return redirect()->back()->with('mensaje', 'Producto agregado a carrito');

    }

    public function EliminarProducto($id)
    {
        $carrito = session()->get('carrito');
        if (isset($carrito[$id])) {

            if ($carrito[$id]["cantidad"] >= 2) {
                $carrito[$id]["cantidad"]--;
                session()->put('carrito', $carrito);
                return redirect()->back()->with('mensaje', 'Una unidad ha sido eliminada');
            } else {
                unset($carrito[$id]);
                session()->put('carrito', $carrito);
                return redirect()->back()->with('mensaje', 'El producto ha sido eliminado por completo');
            }
        }

    }

    public static function MostrarCantidad()
    {

        $carrito = session()->get('carrito');

        if (isset($carrito)) {
            return count($carrito);
        }
        return 0;
    }

    public function CrearOrden($total)
    {
        $ordenNueva = new App\Ordene;

        $ordenNueva->total = $total;
        $ordenNueva->cliente_id = Auth::user()->id;

        $ordenNueva->save();

        return $this->CrearDetalles();

    }

    public function CrearDetalles()
    {

        $carrito = session()->get('carrito');
        $orden = App\Ordene::Orderby('id', 'desc')->limit(1)->get()->pluck('id');
        $nuevoDetalle = new App\Detalle;

        if (isset($carrito)) {
            foreach (session('carrito') as $id => $item) {

                App\Detalle::create([
                    'producto_id' => $id,
                    'orden_id' => $orden[0],
                    'cantidad' => $item['cantidad'],
                ]);
            }
            return $this->ActualizarStock();

        }
    }


    public function ActualizarStock()
    {
        $carrito = session()->get('carrito');
        if (isset($carrito)) {
            foreach (session('carrito') as $id => $item) {

                DB::update('UPDATE productos SET stock = stock - ? WHERE id = ?', [$item['cantidad'],$id]);
            }
            $sesion = session()->forget('carrito');
            return redirect()->route('shop')->with('mensaje','Compra realizada con éxito');

        }
    }


    
    public static function MontoTotalGastado()
    {
        $id = Auth::user()->id;
        $query = DB::table("ordenes")->where('cliente_id','=',$id)->sum('total');
        return $query;
    }

    public static function CantidadDeProductos(){
        $id = Auth::user()->id;
        $query = DB::table('ordenes')->join('detalles','ordenes.id','=','detalles.orden_id')->where('ordenes.cliente_id','=',$id)->count();
        return $query;
    }


    public function MirarOrdenesRealizadas()
    {
        $id = Auth::user()->id;

        $ordenes = App\Ordene::where('cliente_id', $id)->paginate(5);
        return view('client.pedidos',compact('ordenes'));
    }

    public function MirarDetallesOrden($id)
    {
        $detalles = DB::table('ordenes')->
        join('detalles','ordenes.id','=','detalles.orden_id')->
        join('productos','detalles.producto_id','=','productos.id')->where('detalles.orden_id', '=' , $id)->
        select('productos.descripcion','detalles.cantidad','productos.imagen','productos.precio')->get();

        return view('client.infoPedidos',compact('detalles'));
        
    }

    public function MirarDetalleProducto($id)
    {
        $producto = App\Producto::findOrFail($id);
        return view('client.detalleProducto',compact('producto'));
    }
}
