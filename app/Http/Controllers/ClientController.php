<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App;

class ClientController extends Controller
{
    public function Inicio()
    {
        return view('client.inicio');
    }

    public function MirarProductosHombre($id)
    {
        $productos = App\Producto::where('categoria_id','=', $id)->where('genero_id','=','1')->paginate(3);
        return view('client.producto', compact('productos'));
    }

    public function MirarProductosMujer($id)
    {
        $productos = App\Producto::where('categoria_id','=', $id)->where('genero_id','=','2')->paginate(3);
        return view('client.producto', compact('productos'));
    }

}
