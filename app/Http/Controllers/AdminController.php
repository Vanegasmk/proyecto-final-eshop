<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function Inicio()
    {
        $personasRegistradas = App\User::where('rol_id', 2)->count();
        $productosVendidos = App\Detalle::all()->count();
        $totalVentas = App\Ordene::all()->sum('total');

        return view('admin.inicio', compact('personasRegistradas','productosVendidos','totalVentas'));
    }

    public function MirarUsuarios()
    {
        $personas = App\User::where('rol_id', 2)->paginate(10);
        return view('admin.usuarios.usuarios', compact('personas'));
    }

    public function EliminarUsuario($id)
    {
        $personaEliminar = App\User::findOrFail($id);
        $personaEliminar->delete();

        return back()->with('mensaje', 'Usuario Eliminado');
    }

    public function EditarUsuario($id)
    {
        $persona = App\User::findOrFail($id);
        return view('admin.usuarios.editar', compact('persona'));
    }

    public function ActualizarUsuario(Request $request, $id)
    {
        $usuarioActualizar = App\User::findOrFail($id);
        $usuarioActualizar->name = $request->name;
        $usuarioActualizar->last_name = $request->last_name;
        $usuarioActualizar->direction = $request->direction;
        $usuarioActualizar->phone_number = $request->phone_number;

        $usuarioActualizar->save();

        return redirect()->route('usuarios.mirar')->with('mensaje', 'Usuario Actualizado');
    }

    public function MirarCategorias()
    {
        $categorias = App\Categoria::paginate(10);
        return view('admin.categorias.categorias', compact('categorias'));
    }

    public function vistaCategoria()
    {
        return view('admin.categorias.crear');
    }

    public function CrearCategoria(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
        ]);

        $categoriaNueva = new App\Categoria;

        $categoriaNueva->nombre = $request->nombre;

        $categoriaNueva->save();

        return redirect()->route('categorias.mirar')->with('mensaje', 'Categoría Agregada');
    }

    public function EditarCategoria($id)
    {
        $categoria = App\Categoria::findOrFail($id);
        return view('admin.categorias.editar', compact('categoria'));
    }

    public function EliminarCategoria($id)
    {
        try {
            $personaEliminar = App\Categoria::findOrFail($id);
            $personaEliminar->delete();

            return back()->with('mensaje', 'Categoría Eliminada');
        } catch (Exception $e) {
            return back()->with('error', 'No se puede eliminar debido a que es usada como llave foránea');
        }
    }

    public function ActualizarCategoria(Request $request, $id)
    {
        $categoriaActualizar = App\Categoria::findOrFail($id);
        $categoriaActualizar->nombre = $request->nombre;

        $categoriaActualizar->save();

        return redirect()->route('categorias.mirar')->with('mensaje', 'Categoría Actualizada');
    }


    public function MirarProductos()
    {
        $productos = App\Producto::paginate(3);
        return view('admin.productos.productos',compact('productos'));
    }

    public function VistaProducto()
    {
        $categorias = App\Categoria::all();
        $generos = App\Genero::all();
        return view('admin.productos.crear', compact('categorias', 'generos'));
    }

    public function CrearProducto(Request $request)
    {
        try {
            $request->validate([
                'nombre' => 'required|string',
                'precio' => 'required|numeric',
                'descripcion' => 'required|string',
                'imagen' => 'required|image|mimes:jpeg,png,jpg,svg',
                'stock' => 'required|numeric|min:1',
                'categoria_id' => 'required|numeric|min:1',
                'genero_id' => 'required|numeric|min:1',
    
            ]);
    
            $productoNuevo = new App\Producto;
    
            $productoNuevo->nombre = $request->nombre;
            $productoNuevo->descripcion = $request->descripcion;
            $nombreImagen = time(). '.' .$request->imagen->getClientOriginalExtension();
            
            $productoNuevo->imagen = $nombreImagen;
            $request->imagen->move(public_path('imagenes'),$nombreImagen);
            
            $productoNuevo->precio = $request->precio;
            $productoNuevo->stock = $request->stock;
            $productoNuevo->categoria_id = $request->categoria_id;
            $productoNuevo->genero_id = $request->genero_id;
    
            $productoNuevo->save();

            return redirect()->route('productos.mirar')->with('mensaje','Producto Agregado');
        } catch (Exception $e) {
            return $e;
        }
    }

    public function EditarProducto($id)
    {
        $categorias = App\Categoria::all();
        $generos = App\Genero::all();
        $producto = App\Producto::findOrFail($id);
        return view('admin.productos.editar', compact('producto','categorias','generos'));
    }

    public function ActualizarProducto(Request $request,$id)
    {
        try {
            if($request->has('insertcheck')){
                
                $request->validate([
                    'nombre' => 'required|string',
                    'precio' => 'required|numeric',
                    'descripcion' => 'required|string',
                    'stock' => 'required|numeric|min:1',
                    'categoria_id' => 'required|numeric|min:1',
                    'genero_id' => 'required|numeric|min:1',
        
                ]);
        
                $productoNuevo =  App\Producto::findOrFail($id);
        
                $productoNuevo->nombre = $request->nombre;
                $productoNuevo->descripcion = $request->descripcion;
                $productoNuevo->precio = $request->precio;
                $productoNuevo->stock = $request->stock;
                $productoNuevo->categoria_id = $request->categoria_id;
                $productoNuevo->genero_id = $request->genero_id;
        
                $productoNuevo->save();
    
                return redirect()->route('productos.mirar')->with('mensaje','Producto Actualizado');
            }else{
                $request->validate([

                    'nombre' => 'required|string',
                    'precio' => 'required|numeric|min:1',
                    'descripcion' => 'required|string',
                    'stock' => 'required|numeric|min:1',
        
                ]);
        
                $productoNuevo =  App\Producto::findOrFail($id);
        
                $productoNuevo->nombre = $request->nombre;
                $productoNuevo->descripcion = $request->descripcion;
                $productoNuevo->precio = $request->precio;
                $productoNuevo->stock = $request->stock;
                $productoNuevo->categoria_id = $productoNuevo->categoria_id;
                $productoNuevo->genero_id = $productoNuevo->genero_id;
        
                $productoNuevo->save();
    
                return redirect()->route('productos.mirar')->with('mensaje','Producto Actualizado');
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    public function EliminarProducto($id)
    {
        $productoEliminar = App\Producto::findOrFail($id);
        $productoEliminar->delete();
        return back()->with('mensaje', 'Producto Eliminado');

    }

    public static function ProductosVendidos()
    {
        $productosVendidos = App\Detalle::all()->count();

    }
}
