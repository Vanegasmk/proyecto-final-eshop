@extends('layouts.dashadmin')

@section('content')

@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Productos</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" with="100%" cellspacing="0" id="dataTable">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Precio</th>
                        <th>Stock</th>
                        <th>Género</th>
                        <th>Imagen</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($productos as $producto)
                    <tr>
                        <th>{{$producto->nombre}}</th>
                        <th>{{$producto->descripcion}}</th>
                        <th>₡{{$producto->precio}}</th>
                        <th>{{$producto->stock}}</th>
                        <th>
                            @if ($producto->genero_id == 1)
                                Hombre
                            @else
                                Mujer
                            @endif
                        </th>
                        <th><center><img src="{{asset('/imagenes/'.$producto->imagen)}}" height="100px"></center></th>
                        <td>
                        <a href="{{route('productos.editar',$producto->id)}}" class="btn btn-primary btn-sm">Editar</a>

                            <form onclick="return confirm('¿Desea eliminarlo?')"
                                action="{{route('productos.eliminar',$producto)}}" method="POST" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger btn-sm">Eliminar</button>
                            </form>
                        </td>



                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$productos->links()}}
        </div>
    </div>


</div>



@endsection