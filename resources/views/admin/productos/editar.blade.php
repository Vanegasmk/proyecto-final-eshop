@extends('layouts.dashadmin')


@section('content')
<div class="container">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Editar Categoría</h6>
        </div>

        <div class="card o-hidden border-0 shadow-lg my-5">

            <div class="card-body p-2">

                <div class="p-5">

                    <form action="{{route('productos.actualizar',$producto->id)}}" class="user" method="POST">
                        @method('PUT')
                        @csrf


                        <div class="form-group row">

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Nombre</label>
                                <input type="text" value="{{$producto->nombre}}" class="form-control" name="nombre"
                                    required>
                                {!!$errors->first('nombre','<small>:message </small> <br>')!!}
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Descripción</label>
                                <input type="text" value="{{$producto->descripcion}}" class="form-control"
                                    name="descripcion" required>
                                {!!$errors->first('descripcion','<small>:message </small> <br>')!!}
                            </div>

                        </div>


                        <div class="from-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Precio</label>
                                <input type="number" value="{{$producto->precio}}" class="form-control" min="1"
                                    name="precio" required>
                                {!!$errors->first('precio','<small>:message </small> <br>')!!}
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Stock</label>
                                <input type="number" value="{{$producto->stock}}" class="form-control" min="1"
                                    name="stock" required>
                                {!!$errors->first('stock','<small>:message </small> <br>')!!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Categoría</label>
                                <select name="categoria_id" id="categoria_id" class="form-control" required disabled>
                                    <option disabled value="{{$producto->categoria_id}}" selected></option>
                                    <optgroup label="Nueva Opción">
                                        @foreach ($categorias as $categoria)
                                        <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                        @endforeach
                                    </optgroup>
                                    {!!$errors->first('categoria','<small>:message </small> <br>')!!}
                                </select>
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Género</label>
                                <select name="genero_id" class="form-control" id="genero_id" required disabled>
                                    <option disabled value="{{$producto->genero_id}}" seleted></option>
                                    <optgroup label="Nueva Opción">
                                        @foreach ($generos as $genero)
                                        <option value="{{$genero->id}}">{{$genero->nombre}}</option>
                                        @endforeach
                                    </optgroup>
                                    {!!$errors->first('genero','<small>:message </small> <br>')!!}
                                </select>
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <div class="form-check">
                                    
                                    <input type="checkbox" class="form-check-input" id="insertcheck" name="insertcheck">
                                    <label class="form-check-label" for="exampleCheck1">Actualizar todos los
                                        datos</label>
                                </div>
                            </div>
                            <script>
                                document.getElementById('insertcheck').onchange = function() {
                                document.getElementById('genero_id').disabled = !this.checked;
                                document.getElementById('categoria_id').disabled = !this.checked;
                                };
                            </script>


                            <button class="btn btn-primary btn-lg btn-block" type="submit">Editar</button>
                    </form>

                </div>

            </div>

        </div>

    </div>
</div>
@endsection