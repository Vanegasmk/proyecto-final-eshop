@extends('layouts.dashadmin')


@section('content')
<div class="container">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Crear Producto</h6>
        </div>

        <div class="card o-hidden border-0 shadow-lg my-5">

            <div class="card-body p-2">

                <div class="p-5">

                    <form action="{{route('productos.crear')}}" class="user" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="nombre" required>
                                {!!$errors->first('nombre','<small>:message </small> <br>')!!}
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Descripción</label>
                                <input type="text" class="form-control" name="descripcion" required>
                                {!!$errors->first('descripcion','<small>:message </small> <br>')!!}
                            </div>

                        </div>


                        <div class="from-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Precio</label>
                                <input type="number" class="form-control" min="1" name="precio" required>
                                {!!$errors->first('precio','<small>:message </small> <br>')!!}
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Stock</label>
                                <input type="number" class="form-control" min="1" name="stock" required>
                                {!!$errors->first('stock','<small>:message </small> <br>')!!}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Categoría</label>
                                <select name="categoria_id" class="form-control" required>
                                    <option value="" selected></option>
                                    @foreach ($categorias as $categoria)
                                    <option value="{{$categoria->id}}">{{$categoria->nombre}}</option>
                                    @endforeach
                                    {!!$errors->first('categoria','<small>:message </small> <br>')!!}
                                </select>
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Género</label>
                                <select name="genero_id" class="form-control">
                                    <option value="" selected></option>
                                    @foreach ($generos as $genero)
                                    <option value="{{$genero->id}}">{{$genero->nombre}}</option>
                                    @endforeach
                                    {!!$errors->first('genero','<small>:message </small> <br>')!!}
                                </select>
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Imagen</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="imagen" required>
                                    <label class="custom-file-label" for="customFile">Seleccionar Archivo</label>
                                    {!!$errors->first('imagen','<small>:message </small> <br>')!!}
                                </div>
                            </div>

                        </div>
                        <hr>
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Crear</button>
                    </form>

                </div>

            </div>

        </div>

    </div>
</div>
@endsection