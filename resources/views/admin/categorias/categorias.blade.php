@extends('layouts.dashadmin')



@section('content')
@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif

@if (session('alerta'))
<div class="alert alert-danger"">

    {{session('alerta')}}

</div>
@endif
<div class=" card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Categorías</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" with="50%" cellspacing="0" id="dataTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Creado</th>
                        <th>Actualizado</th>
                        <th>Acciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($categorias as $categoria)
                    <tr>
                        <th>{{$categoria->id}}</th>
                        <th>{{$categoria->nombre}}</th>
                        <th>{{$categoria->created_at}}</th>
                        <th>{{$categoria->updated_at ?? 'No ha sido actualizado'}}</th>

                        <td>
                            <a href="{{route('categorias.editar',$categoria)}}" class="btn btn-primary btn-sm">Editar</a>

                            <form onclick="return confirm('¿Desea eliminarlo?')" action="{{route('categorias.eliminar',$categoria)}}" method="POST" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger btn-sm">Eliminar</button>
                            </form>
                        </td>



                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$categorias->links()}}
        </div>
    </div>


</div>



@endsection