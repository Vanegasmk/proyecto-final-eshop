@extends('layouts.dashadmin')


@section('content')
<div class="container">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Crear Categoría</h6>
        </div>

        <div class="card o-hidden border-0 shadow-lg my-5">

            <div class="card-body p-2">

                <div class="p-5">

                    <form action="{{route('categorias.crear')}}" class="user" method="POST">
                        @csrf

                        <div class="form-group row">

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Nombre</label>
                                <input type="text"  class="form-control" name="nombre" required>
                                {!!$errors->first('nombre','<small>:message </small> <br>')!!}
                            </div>

                        </div>
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Crear</button>
                    </form>

                </div>

            </div>

        </div>

    </div>
</div>
@endsection