@extends('layouts.dashadmin')

@section('content')

@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Usuarios</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" with="100%" cellspacing="0" id="dataTable">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Teléfono</th>
                        <th>Correo</th>
                        <th>Dirección</th>
                        <th>Acciones</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ($personas as $persona)
                    <tr>
                        <th>{{$persona->name}}</th>
                        <th>{{$persona->last_name}}</th>
                        <th>{{$persona->phone_number}}</th>
                        <th>{{$persona->email}}</th>
                        <th>{{$persona->direction}}</th>


                        <td>
                            <a href="{{route('usuarios.editar',$persona)}}" class="btn btn-primary btn-sm">Editar</a>

                            <form onclick="return confirm('¿Desea eliminarlo?')" action="{{route('usuarios.eliminar',$persona)}}" method="POST" class="d-inline">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-danger btn-sm">Eliminar</button>
                            </form>
                        </td>



                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{$personas->links()}}
        </div>
    </div>


</div>



@endsection