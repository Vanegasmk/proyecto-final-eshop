@extends('layouts.dashadmin')


@section('content')
<div class="container">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Editar Usuario</h6>
        </div>

        <div class="card o-hidden border-0 shadow-lg my-5">

            <div class="card-body p-2">

                <div class="p-5">

                    <form action="{{route('usuarios.actualizar',$persona->id)}}" class="user" method="POST">
                        @method('PUT')
                        @csrf

                        <div class="form-group row">

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Nombre</label>
                                <input type="text" value="{{$persona->name}}" class="form-control" name="name" required>
                                {!!$errors->first('name','<small>:message </small> <br>')!!}
                            </div>

                            <div class="col-sm-6">
                                <label>Apellidos</label>
                                <input value="{{$persona->last_name}}" type="text" class="form-control" name="last_name"
                                    required>
                                {!!$errors->first('last_name','<small>:message </small> <br>')!!}
                            </div>

                        </div>

                        <div class="form-group row">

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Dirección</label>
                                <input type="text" value="{{$persona->direction}}" class="form-control" name="direction"
                                    required>
                                {!!$errors->first('direction','<small>:message </small> <br>')!!}
                            </div>

                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <label>Teléfono</label>
                                <input type="text" value="{{$persona->phone_number}}" class="form-control" name="phone_number"
                                    required>
                                {!!$errors->first('phone_number','<small>:message </small> <br>')!!}
                            </div>

                        </div>
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Editar</button>
                    </form>

                </div>

            </div>

        </div>
        
    </div>
</div>
@endsection