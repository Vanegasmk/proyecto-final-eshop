<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script>

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('shop')}}">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-tags"></i>
                </div>
                <div class="sidebar-brand-text mx-3">{{__('Eshop')}}</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                {{__('Categorías')}}
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-male"></i>
                    <span>{{__('Hombres')}}</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header"><i class="fas fa-tshirt"></i> {{__('Ropa')}}</h6>
                        <a class="collapse-item" href="{{route('mirar.camisas.hombre',1)}}">{{__('Camisas')}}</a>
                        <a class="collapse-item" href="{{route('mirar.pantalones.hombre',2)}}">{{__('Pantalones')}}</a>
                    </div>
                </div>
            </li>

            <!-- Nav Item - Utilities Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-female"></i>
                    <span>{{__('Mujer')}}</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header"><i class="fas fa-tshirt"></i> {{__('Ropa')}}</h6>
                        <a class="collapse-item" href="{{route('mirar.camisas.mujer',1)}}">{{__('Camisas')}}</a>
                        <a class="collapse-item" href="{{route('mirar.pantalones.mujer',2)}}">{{__('Pantalones')}}</a>
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">

            <a class="nav-link" href="{{route('carrito.ordenes')}}">
                    <i class="fas fa-shopping-bag"></i>
                    &nbsp;
                    <span>{{__('Pedidos Realizados')}}</span>
                </a>

            </li>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Messages -->
                        <li class="nav-item dropdown no-arrow mx-1">
                            <a class="nav-link dropdown-toggle" href="{{route('mirar.carrito')}}">
                                <i class="fas fa-shopping-bag"></i>
                                <!-- Counter - Messages -->
                                <span class="badge badge-danger badge-counter">
                                    @php
                                        echo App\Http\Controllers\CarritoController::MostrarCantidad();
                                    @endphp
                                </span>
                            </a>
                        </li>

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{__('Bienvenido')}} {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar Sesión') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    @yield('content')
                </div>

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->



        <!-- Bootstrap core JavaScript-->
        <script src="asset{{'jquery/jquery.min.js'}}"></script>
        <script src="asset{{'bootstrap/js/bootstrap.bundle.min.js'}}"></script>

        <!-- Core plugin JavaScript-->
        <script src="asset{{'jquery-easing/jquery.easing.min.js'}}"></script>

        <!-- Custom scripts for all pages-->
        <script src="asset{{'js/sb-admin-2.min.js'}}"></script>

        <!-- Page level plugins -->
        <script src="asset{{'chart.js/Chart.min.js'}}"></script>

        <!-- Page level custom scripts -->
        <script src="asset{{'js/demo/chart-area-demo.js'}}"></script>
        <script src="asset{{'js/demo/chart-pie-demo.js'}}"></script>

</body>

</html>