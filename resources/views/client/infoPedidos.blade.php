@extends('layouts.dashshop')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Pedidos Realizados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" with="50%" cellspacing="0" id="dataTable">
                <thead>
                    <tr>
                        <th>Descripción</th>
                        <th>Cantidad</th>
                        <th>Imagen</th>
                        <th>Precio</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($detalles as $detalle)
                    <tr>
                        <th>
                            {{$detalle->descripcion}}
                        </th>
                        <th>
                            {{$detalle->cantidad}}
                        </th>
                        <th>
                            <img src="{{asset('/imagenes/'.$detalle->imagen)}}" height="70px">
                        </th>
                        <th>
                            {{$detalle->precio}}
                        </th>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
@endsection