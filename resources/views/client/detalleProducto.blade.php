@extends('layouts.dashshop')

@section('content')
@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif
<div class="container">
    <div class="row">
        <div class="container-fluid">
            <div class="card w-50">
                <img src="{{asset('/imagenes/'.$producto->imagen)}}" class="card-img-top" />
            </div>

            <div class="card-body">
                <h5 class="card-title">{{$producto->nombre}}</h5>
                <p class="card-text">{{$producto->descripcion}}</p>
                <p class="card-text">Cantidad en stock: {{$producto->stock}}</p>
                <p class="card-text">Precio: ₡{{$producto->precio}}</p>
                <a class="btn btn-success btn-icon-split" href="{{route('carrito.agregar',$producto->id)}}">
                    <span class="icon text-white-50">
                        <i class="fas fa-money-check"></i>
                    </span>
                    <span class="text">Añadir a Carrito</span>
        
                </a>
            </div>
        </div>
    </div>
</div>
</div>




@endsection