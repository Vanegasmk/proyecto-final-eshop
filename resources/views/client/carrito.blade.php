@extends('layouts.dashshop')

@section('content')

@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif


@if (session('carrito'))

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Carrito</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" with="50%" cellspacing="0" id="dataTable">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Imagen</th>
                        <th>Cantidad</th>
                        <th>Precio</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                @php
                $total = 0;

                foreach (session('carrito') as $id => $item) {
                $total += $item["precio"] * $item["cantidad"];
                }
                @endphp

                @foreach (session('carrito') as $id => $item)
                <tbody>
                    <th>
                        {{$item['nombre']}}
                    </th>
                    <th>
                        {{$item['descripcion']}}
                    </th>
                    <th>
                        <img src="{{asset('/imagenes/'.$item['imagen'])}}" height="70px">
                    </th>
                    <th>
                        {{$item['cantidad']}}
                    </th>
                    <th>
                        ₡{{$item['precio']}}
                    </th>
                    <th>
                        <form class="text-center">

                            <a onclick="return confirm('¿Desea eliminarlo?')" class="btn btn-danger"
                                href="{{route('carrito.eliminar',$id)}}">
                                <i class="fas fa-trash"></i>
                                Eliminar
                            </a>

                        </form>
                    </th>
                </tbody>
                @endforeach
                <tfoot>
                    <tr>
                        <td colspan="4" class="text-right">Total</td>
                        <td>₡{{$total}}</td>
                        <td>

                            <center>


                            <a class="btn btn-info" id="botonPagar" href="{{route('carrito.crear.orden',$total)}}">
                                    <i class="fas fa-money-bill-wave-alt"></i>
                                    Pagar
                                </a>


                            </center>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@else
<div class="alert alert-warning">

    No hay contenido agregado al carrito

</div>
@endif
@endsection