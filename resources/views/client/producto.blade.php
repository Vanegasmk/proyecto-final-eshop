@extends('layouts.dashshop')

@section('content')

@if (session('mensaje'))
<div class="alert alert-success">

    {{session('mensaje')}}

</div>
@endif

<div class="container">
    <div class="row">
        @foreach ($productos as $item)
        <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
                <a href="#"><img class="card-img-top"  src="{{asset('/imagenes/'.$item->imagen)}} "></a>
                <div class="card-body">
                    <h4 class="card-title">
                    <a href="{{route('producto.detalles',$item->id)}}">{{$item->nombre}}</a>
                    </h4>
                    <p>Precio</p>
                    <h5>₡{{$item->precio}}</h5>
                <p>{{$item->descripcion}}</p>
                </div>
                <div class="card-footer">
                    <center>
                    <a class="btn btn-success btn-icon-split" href="{{route('carrito.agregar',$item->id)}}">
                            <span class="icon text-white-50">
                                <i class="fas fa-money-check"></i>
                            </span>
                            <span class="text">Añadir a Carrito</span>

                        </a>
                    </center>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{$productos->links()}}
</div>
@endsection