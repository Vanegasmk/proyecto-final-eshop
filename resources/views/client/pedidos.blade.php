@extends('layouts.dashshop')


@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Pedidos Realizados</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" with="50%" cellspacing="0" id="dataTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fecha Compra</th>
                        <th>Total</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($ordenes as $orden)
                    <tr>
                        <th>
                            {{$orden->id}}
                        </th>
                        <th>
                            {{$orden->created_at}}
                        </th>
                        <th>
                            {{$orden->total}}
                        </th>
                        <th>
                            <form class="text-center">

                                <a  class="btn btn-info"
                            href="{{route('carrito.ordenes.detalles',$orden->id)}}">
                                    <i class="fas fa-eye"></i>
                                    Mirar Detalle
                                </a>

                            </form>
                        </th>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
@endsection