@extends('layouts.authr')

@section('form')

<div class="text-center">
    <h1 class="h4 text-gray-900 mb-4">{{ __('¡Registrate!') }}</h1>
</div>
<form method="POST" class="user" action="{{ route('register') }}">
    @csrf

    <div class="form-group row">

        <div class="col-sm-6 mb-3 mb-sm-0">

            <input id="name" type="text" class="form-control form-control-user @error('name') is-invalid @enderror"
                name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre">

            @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>

        <div class="col-sm-6">

            <input id="last_name" type="text"
                class="form-control form-control-user @error('last_name') is-invalid @enderror" name="last_name"
                value="{{ old('last_name') }}" required autocomplete="name" autofocus placeholder="Apellidos">

            @error('last_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>
    </div>


    <div class="form-group">
        <input id="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror"
            name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo">

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <hr>

    <div class="form-group row">

        <div class="col-sm-6">

            <input id="password" type="password"
                class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required
                autocomplete="new-password" placeholder="Contraseña">

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>

        <div class="col-sm-6 mb-3 mb-sm-0">

            <input id="password-confirm" type="password" class="form-control form-control-user"
                name="password_confirmation" required autocomplete="new-password" placeholder="Confirmar Contraseña">

        </div>

    </div>

    <div class="form-group row">

        <div class="col-sm-6 mb-3 mb-sm-0">

            <input id="phone_number" type="text" class="form-control form-control-user @error('phone_number') is-invalid @enderror"
                name="phone_number" value="{{ old('phone_number') }}" required autocomplete="name" autofocus placeholder="Teléfono">

            @error('phone_number')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>

        <div class="col-sm-6">

            <input id="direction" type="text"
                class="form-control form-control-user @error('direction') is-invalid @enderror" name="direction"
                value="{{ old('direction') }}" required autocomplete="name" autofocus placeholder="Dirección">

            @error('direction')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror

        </div>
    </div>

    <hr>

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-3">
            <button type="submit" class="btn btn-primary btn-user btn-block">
                {{ __('Registrarse') }}
            </button>
        </div>
    </div>
</form>
@endsection