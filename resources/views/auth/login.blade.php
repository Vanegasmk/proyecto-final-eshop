@extends('layouts.authl')

@section('form')
<br><br><br><br>
<div class="text-center">
    <h1 class="h4 text-gray-900 mb-4">{{ __('¡Bienvenido!') }}</h1>
</div>

<form class="user" method="POST" action="{{route('login')}}">
    @csrf
    <div class="form-group">
        <input id="email" type="email" class="form-control form-control-user @error('email') is-invalid @enderror"
            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Correo">

        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    <div class="form-group">
        <input id="password" type="password"
            class="form-control form-control-user @error('password') is-invalid @enderror" name="password" required
            autocomplete="current-password" placeholder="Clave">


        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>


    <div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            {{ __('Iniciar Sesión') }}
        </button>
    </div>

</form>

<hr>

<div class="text-center">
    <a class="small" href="{{ route('register') }}">Crea una cuenta</a>
</div>
@endsection